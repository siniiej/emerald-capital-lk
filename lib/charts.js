var mainChart = document.getElementById("mainChart");
var tokensAmount = new Chart(mainChart, {
  type: 'bar',
  data: {
    labels: ["04.06", "11", "18", "25", "02.07", "9", "16", "23", "30", "02.08", "9", "16", "23", "30"],
    datasets: [{
      label: "Количество токенов на балансе",
      data: [12, 19, 3, 5, 2, 3, 7, 9, 12, 15, 3, 5, 18, 14],
      backgroundColor: 'rgba(230, 160, 55, 0.6)',
      borderColor: 'rgb(230, 160, 55)',
      borderWidth: 1
    }]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontFamily: "Muller",
          fontColor: "#000"
        }
      }],
      xAxes: [{
        ticks: {
          fontFamily: "Muller",
          fontColor: "#000"
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        }
      }]
    },
    legend: {
      display: false,
      position: "top"
    },
    tooltips: {
      cornerRadius: 0,
      bodyFontFamily: "Muller",
      titleFontFamily: "Muller",
      xPadding: 10,
      yPadding: 8,
      displayColors: false
    }
  }
});

var necPrice = document.getElementById("necPrice");
var necChart = new Chart(necPrice, {
  type: 'line',
  data: {
    labels: ["04.06", "11", "18", "25", "02.07", "9", "16", "23", "30", "02.08", "9", "16", "23", "30"],
    datasets: [{
      label: "Количество токенов на балансе",
      data: [12, 19, 3, 5, 2, 3, 7, 9, 12, 15, 3, 5, 18, 14],
      backgroundColor: 'rgba(230, 160, 55, 0.6)',
      borderColor: 'rgb(230, 160, 55)',
      borderWidth: 1
    }]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontFamily: "Muller",
          fontColor: "#000"
        }
      }],
      xAxes: [{
        ticks: {
          fontFamily: "Muller",
          fontColor: "#000"
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        }
      }]
    },
    legend: {
      display: false,
      position: "top"
    },
    tooltips: {
      cornerRadius: 0,
      bodyFontFamily: "Muller",
      titleFontFamily: "Muller",
      xPadding: 10,
      yPadding: 8,
      displayColors: false
    }
  }
});

var returnChart = document.getElementById("returnChart");
var returnRateChart = new Chart(returnChart, {
  type: 'line',
  data: {
    labels: ["04.06", "11", "18", "25", "02.07", "9", "16", "23", "30", "02.08", "9", "16", "23", "30"],
    datasets: [{
      label: "Средняя доходность в месяц",
      data: [12, 19, 3, 5, 2, 3, 7, 9, 12, 15, 3, 5, 18, 14],
      backgroundColor: 'rgba(230, 160, 55, 0.6)',
      borderColor: 'rgb(230, 160, 55)',
      borderWidth: 1
    }, {
      label: "Абсолютная доходность",
      data: [2, 16, 5, 7, 9, 14, 7, 2, 9, 3, 8, 1, 7, 12],
      backgroundColor: 'rgba(38,255,99, 0.6)',
      borderColor: '#26FF63',
      borderWidth: 1
    }]
  },
  options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          fontFamily: "Muller",
          fontColor: "#000"
        }
      }],
      xAxes: [{
        ticks: {
          fontFamily: "Muller",
          fontColor: "#000"
        },
        gridLines: {
          display: false,
          color: "rgba(241, 241, 241, 0.5)"
        }
      }]
    },
    legend: {
      display: true,
      position: "bottom"
    },
    tooltips: {
      cornerRadius: 0,
      bodyFontFamily: "Muller",
      titleFontFamily: "Muller",
      xPadding: 10,
      yPadding: 8,
      displayColors: false
    }
  }
});